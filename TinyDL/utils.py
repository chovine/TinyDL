def map_nested_lists(L1, L2, func):
    if not isinstance(L1, list):
        return func(L1, L2)
    
    Lout = list()
    for l1, l2 in zip(L1, L2):
        Lout.append(map_nested_lists(l1, l2, func))

    return Lout

def flatten_array(L):
    if not isinstance(L, list):
        return [L] 

    Out = list()
    for l in L:
        Out = Out + flatten_array(l)

    return Out

