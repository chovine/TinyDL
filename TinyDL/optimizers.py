import jax
from jax import jit
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
from functools import partial
from .utils import *

class Optimizer():
    def __init__(self, cost_func, params=None, model=None, dataloader=None):
        self.cost_func = cost_func
        self.model = model
        self.loss = list()
        if model is not None:
            self.model_func = jax.vmap(model.apply, (None, 0))
            params = self.model.get_params()
        else:
            self.model_func = None
        self.dataloader = dataloader
        self.params = params

    def _full_cost(self, params, x, y):
        if self.model_func is None:
            return self.cost_func(x, y)
        
        pred = self.model_func(params, x)
        return self.cost_func(pred, y)

    def step(self, data_x=None, data_y=None):
        pass

    def run(self, epochs=1):

        for e in range(epochs):
            num_batches = len(self.dataloader)
            data_t = tqdm(self.dataloader, total=num_batches)
            data_t.set_description(f"Epoch {e+1}")
            loss = list()
            for x,y in data_t:
                fval = self.step(data_x=x, data_y=y)
                curr_loss = fval
                loss.append(curr_loss)
                self.loss.append(curr_loss)
                data_t.set_postfix(loss=np.mean(loss))

    def plot_loss(self):
        fig, ax = plt.subplots(figsize=(10,5))
        ax.plot(self.loss)
        ax.set_xlabel("Iteration")
        ax.set_ylabel("Training Loss")
        ax.grid()

    def get_params(self):
        return self.params

    def set_params(self, params):
        self.params = params
        if self.model is not None:
            self.model.set_params(params)

class SGD(Optimizer): 
    def __init__(self, *args, step_size=0.01, **kwargs):
        super().__init__(*args, **kwargs)
        self.step_size = step_size

    def step(self, data_x=None, data_y=None):
        params = self.get_params()
        fval, g = jax.value_and_grad(self._full_cost)(params, data_x, data_y)
        params = jax.tree_map(lambda x,y: x-self.step_size*y, params, g)
        self.set_params(params)
        return fval