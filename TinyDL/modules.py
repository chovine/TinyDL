import jax.numpy as jnp
import numpy as np
from jax import jit
import jax
from functools import partial

class Constraint():
    def project(self, params):
        pass

class OrthogonalFilter(Constraint):
    @partial(jit, static_argnums=(0,))
    def project(self, params):
        filter_list = [p[0] for p in params]
        W = np.stack(filter_list)
        U, _, VT = np.linalg.svd(W, full_matrices=False)
        D = np.matmul(U, VT)
        filter_list = list(D)

        for i in range(len(params)):
            params[i][0] = filter_list[i]
        return params


class Module():
    def __init__(self, constraint=None):
        self.params = 0.0
        self.constraint = constraint
        self.init_params()

    def project(self, params):
        if self.constraint is not None:
            params = self.constraint.project(params)
        return params

    def apply(self, params, x):
        pass

    def get_params(self):
        return self.params

    def set_params(self, params):
        self.params = self.project(params)

    def init_params(self):
        pass

    def __call__(self, x):
        return self.apply(self.get_params(), jnp.asarray(x))


class ConvLayer1D(Module):
    def __init__(self, filt_len, channels_in=1, channels_out=1, bias=False, **kwargs):
        self.filt_len = filt_len
        self.bias = bias
        self.channels_out = channels_out
        self.channels_in = channels_in
        super().__init__(**kwargs)

    @partial(jit, static_argnums=(0,))
    def apply(self, params, x):

        out_chans = list()
        xr = jnp.reshape(x,(self.channels_in, -1))
        M = xr.shape[1]
        N = self.filt_len
        bias = int(self.bias)
        for i in range(self.channels_out):
            out_chans.append(self._single_output_apply(params[0][i, :, :], xr)+bias*params[1][i])

        return jnp.stack(out_chans, axis=0) 

    @partial(jit, static_argnums=(0,))
    def _single_output_apply(self, params, x):
        single_conv = lambda i,acc: acc+jnp.convolve(params[i,:], x[i,:], mode='valid')
        M = x.shape[1]
        N = self.filt_len
        init_val = jnp.zeros((M-N+1,))
        return jax.lax.fori_loop(0, self.channels_in, single_conv, init_val)

    def init_params(self):
        filters = np.random.normal(size=(self.channels_out, self.channels_in, self.filt_len), scale=1/self.filt_len)
        biases = np.zeros((self.channels_out))
        self.set_params([filters, biases])

class MaxPoolLayer(Module):
    def __init__(self, domain_width, padding=False, **kwargs):
        self.domain_width = domain_width
        self.padding = padding
        super().__init__(**kwargs)
    
    @partial(jit, static_argnums=(0,))
    def apply(self, params, x):
        C, N = x.shape
        if self.domain_width is None:
            return jnp.max(x, axis=1)

        if self.padding:
            ext_len = -(N % -self.domain_width)
            x_ext = jnp.concatenate([x, jnp.full((C, ext_len), -np.PINF)], axis=1)
        else:
            disc_len = N % self.domain_width
            x_ext = x[:, :-disc_len]
            
        x_resh = x_ext.reshape((C, -1, self.domain_width))
        return jnp.max(x_resh, axis=2)

class ReLU(Module):
    @partial(jit, static_argnums=(0,))
    def apply(self, params, x):
        return jnp.fmax(0.0, x)

class Sigmoid(Module):
    @partial(jit, static_argnums=(0,))
    def apply(self, params, x):
        return 1/(1+jnp.exp(-x))

class FCLayer(Module):
    def __init__(self, dim_in, dim_out, bias=True, **kwargs):
        self.dim_in = dim_in
        self.bias = bias
        if self.bias:
            self.dim_in += 1
        self.dim_out = dim_out
        super().__init__(**kwargs)

    @partial(jit, static_argnums=(0,))
    def apply(self, params, x):
        if self.bias:
            xb = jnp.append(x, 1)
        else:
            xb = x
        return jnp.matmul(params, xb)

    def init_params(self):
        params = np.random.normal(scale=1/self.dim_in, size=(self.dim_out, self.dim_in))
        if self.bias:
            params[:, -1] = 0.0
        
        self.set_params(params)

class Flatten(Module):
    @partial(jit, static_argnums=(0,))
    def apply(self, params, x):
        return jnp.ravel(x)

class LayerStack(Module):
    def __init__(self, *args, **kwargs):
        self.layers = args
        self.last_xout = None
        super().__init__(**kwargs)

    def apply(self, params, x):

        xout = x
        for param, layer in zip(params, self.layers):
            xout = layer.apply(param, xout)

        return xout

    def get_intermediate_results(self, x):
        xout = x
        outs = [x]
        for param, layer in zip(self.get_params(), self.layers):
            xout = layer.apply(param, xout)
            outs.append(xout)

        return outs 

    def get_params(self):
        return [layer.get_params() for layer in self.layers]

    def set_params(self, params):
        for param, layer in zip(params, self.layers):
            layer.set_params(param)

    def init_params(self):
        for layer in self.layers:
            layer.init_params()

