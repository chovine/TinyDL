import numpy as np

class DataLoader():
    def __init__(self, dataset_x, dataset_y, shuffle=True, batch_size=1):
        self.dataset_x = dataset_x
        self.dataset_y = dataset_y
        self.max_index = len(dataset_x)
        self.batch_size = batch_size
        self.curr_index = 0
        self.shuffle = shuffle
        self._shuffle()
        

    def __iter__(self):
        return self

    def _shuffle(self):
        if self.shuffle:
            sort_ind = np.arange(self.max_index)
            np.random.shuffle(sort_ind)
            self.dataset_x = self.dataset_x[sort_ind]
            self.dataset_y = self.dataset_y[sort_ind]


    def __next__(self):
        last_index = min(self.max_index, self.curr_index + self.batch_size)
        curr_index = self.curr_index
        self.curr_index = last_index
        if self.curr_index >= self.max_index:
            self.curr_index = 0
            self._shuffle()
            raise StopIteration

        return self.dataset_x[curr_index:last_index],self.dataset_y[curr_index:last_index]

    def __len__(self):
        return self.max_index //self.batch_size